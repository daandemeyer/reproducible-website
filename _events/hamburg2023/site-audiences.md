# website meeting

reproducible-builds.org

TL;DR:
    - the homepage is fine for developers
    - but maybe not for other types of users/visitors (powerusers, end users, project managers, ...)
    - doing user testing sounds valuable

## Landing page

- the easier/more accesible the concept of reproducible builds is explained, the better.

- It was discussed whether the buttons "Home" "News" "Documentation" are nice enough:
    one view expressed that it's fine (as you can directly get to the documentation)

- Motto: could be reworded as it should probably include the words "security" ?

- Some visuals to explain the concept
  maybe some diagram showing how sources 
  (represented e.g. as a file icon with "src/*" or "*.c" label)
  are build twice 
  (simply show two computers producing *some* binary output)
  perhaps even some video?

## Categories of visitors

- developer encountering rb for the first time (e.g. because someone sent a patch)
- powerusers that hear about rb and want to learn more
- developers of upstream software looking for docs (so they can avoid common pitfalls); i.e. people who are already convinced of the value of rb
- vendors interested (and potentially fund) in rb
- project managers selling the idea of rb to directors/employees
- end users
- scientists

### Do user testing (per category)
User testing would be preferable over guessing what types of users
might want.

### Different landing page for different users/visitors?

### For technical users
the site is probably mostly fine (there are some 404 links in the docs).
most often devs want to look up documentation (and a short path is preferrable)

### Make the homepage more friendly to non-technical users
starting with a very (easy) introduction.


## Google search terms from actual visitors
most people are searching "reproducible builds" or SOURCE_DATE_EPOCH
=> does not tell us very much

- reproducible builds
- rust reproducible
- SOURCE DATE EPOCH 
- deterministic builds

## "Contact us" section?
There **is** actually links in the fineprint at the bottom for the ML and
"Full contact info" (which links to rb.org/who/).
IRC/Matrix channels? Fediverse handles?

- Where to put it?
  Maybe under "Who is involved?" section of the page?
- Add "Contact us" to the sidebar?

## New "Resources" section
containing Tools, Talks, ...


## Continuous tests should probably be renamed (?)

## Feedback from "users" on the Fedora telegram channel

Q: Hi folks, a quick ask from the reproducible builds community: if you visit https://reproducible-builds.org/, without knowing what reproducible builds are, is the website useful and clear?

Asking in this chat for someone that cant pick together on their own what a reproducible build is is strange xD

To me the explanation seems thorough and clear. I believe it also depends on the knowledge level of the user, if he's a dev or not and so on

If the website is targeted to devs then it's okay
