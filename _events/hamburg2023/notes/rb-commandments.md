RB Ten Commandments

original draft:
    

    Commandments by the church of reproducible builds


1.    Thou shall not record the name of thy maker nor the place of thy making (username, hostname)
2.    Thou shall not record the date nor time of thy making, unless you respect the holy SDE spec (date+time)
3.    Thou shall not use memory without initialization or use memory addresses to decide outcomes (ASLR)
4.    Thou shall do all your work in order - not use filesystem-readdir-order nor random order of hash elements
5.    Thou shall not (gamble and) record random numbers (UUID, private/public key, hash-seed, ASLR)
6.    Thou shall only do one thing at a time or ensure races do no harm (parallelism)
7.    Thou shall not look at build machine processor capabilities
8.    Thou shall not look at build machine benchmarks for optimizations
9.    Thou shall be careful with profile-guided-optimization for it can amplify any sin (non-determinism)
10.   Thou shall keep your workspace env clean of timezones, locales and umasks or ensure they do no harm
11.   Thou shall not access the internet during build (servers can be down, contents can change)
12.   Thou shall take note of your build inputs (versions and/or hashes)


##Notes

will slightly re-order existing entries to cover most common problems first
reword 11th to "allow offline builds"

drop|soften 12th because that is what distributions do in SBOMs "only if you distribute binaries yourself"

#8 and #9 are different, because PGO can be done deterministically and is different from benchmarking

not cover `BUILD_PATH_PREFIX` as builders can use a constant build path with current container tech



## raw notes
* build path: new rule? more in rule 1?
* random tmpdir in binary...
  * part of rule 5, or new subrule?
* consider oder by frequency?
* target audience: upstream source code owners.
  * (implies: don't rant at them about things distro will do, e.g. input manifest style)
* ... how can we communicate "no internet during build (but a fetch phase is fine if it's clear/separate/?)" ... to single package upstreams? (e.g. rule 11)
  * "thou shalt not adulterate other computers during the build" ??
    * weird phrasing, but complicated topic so maybe weird words give pause, and that's appropriate?
* rule 11 += "have well documented fetch directory layour expectation"
  * -> someone else can reasonably provide it.
* rule 11 += "or in pennace thou must make the way clear for other saints and clearhearted neighbors to provide for thy needs in their own clean ways."
* rule 11: just "Thou shall allow offline builds"
* links to theunreproduciblepackage as additional guidance for each item
* are rule 8 & 9 duplicates?
  * disputed, BUT: people talk about "PGO" often enough that we give them a lightning rod.
* rule for clean your cache? might already be obvious.
* `BUILD_PATH_PREFIX` reference in #1 (or in another rule about build paths).
* are 3 and 5 dupe?
  * 5 is things you control obviously
  * 3 deserves callout because it is something the OS surprises you with, so we tell you about it
* move 3 down; it's rarer
