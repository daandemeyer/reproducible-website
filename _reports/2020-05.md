---
layout: report
year: "2020"
month: "05"
title: "Reproducible Builds in May 2020"
draft: false
date: 2020-06-04 14:42:37
---

**Welcome to the May 2020 report from the [Reproducible Builds]({{ "/" | relative_url }}) project.**
{: .lead}

[![]({{ "/images/reports/2020-05/reproducible-builds.png#right" | relative_url }})]({{ "/" | relative_url }})

One of the original promises of open source software is that distributed peer review and transparency of process results in enhanced end-user security. Nonetheless, whilst anyone may inspect the source code of free and open source software for malicious flaws, almost all software today is distributed as pre-compiled binaries. This allows nefarious third-parties to compromise systems by injecting malicious code into seemingly secure software during the various compilation and distribution processes.

In these reports we outline the most important things that we and the rest of the community have been up to over the past month.

## News

The [Corona-Warn](https://www.coronawarn.app/en/) app that helps trace infection chains of SARS-CoV-2/COVID-19 in Germany had a [feature request filed against it that it build reproducibly](https://github.com/corona-warn-app/cwa-documentation/issues/14).

A number of academics from [Cornell University](https://www.cornell.edu/) have published a paper titled [*Backstabber's Knife Collection*](https://arxiv.org/abs/2005.09535) which reviews various open source software supply chain attacks:

> Recent years saw a number of supply chain attacks that leverage the increasing use of open source during software development, which is facilitated by dependency managers that automatically resolve, download and install hundreds of open source packages throughout the software life cycle.

In related news, the [LineageOS](https://lineageos.org/) Android distribution announced that a [hacker had access to the infrastructure of their servers](https://twitter.com/LineageAndroid/status/1256821056100163584) after exploiting an unpatched vulnerability.

Marcin Jachymiak of the [Sia](https://sia.tech/) decentralised cloud storage platform posted on their blog that their `siac` and `siad` utilities can now be built reproducibly:

> This means that anyone can recreate the same binaries produced from our official release process. Now anyone can verify that the release binaries were created using the source code we say they were created from. No single person or computer needs to be trusted when producing the binaries now, which greatly reduces the attack surface for Sia users.

[Synchronicity](https://github.com/iqlusioninc/synchronicity) is a distributed build system for [Rust](https://www.rust-lang.org/) build artifacts which have been published to [crates.io](https://crates.io/). The goal of *Synchronicity* is to provide a [distributed binary](https://wiki.mozilla.org/Security/Binary_Transparency) transparency system which is independent of any central operator.

The [*Comparison of Linux distributions*](https://en.wikipedia.org/wiki/Comparison_of_Linux_distributions) article on Wikipedia now features a *Reproducible Builds* column indicating whether distributions approach and progress towards achieving reproducible builds.

<br>

## Distribution work

In Debian this month:

[![]({{ "/images/reports/2020-05/debian.png#right" | relative_url }})](https://debian.org/)

* [Paul Wise](https://bonedaddy.net/pabs3/) continued a discussion that was started in February regarding the [storing and distribution of build logs and other related artifacts](https://bugs.debian.org/950585) and their relationship to reproducible builds. For example, the `binutils` package ships its own, unreproducible, log files in its binary packages. It was followed-up by replies from Chris Lamb and Matthias Klose.

* 34 reviews of Debian packages were added, 20 were updated and 122 were removed this month adding to [our knowledge about identified issues](https://tests.reproducible-builds.org/debian/index_issues.html). Chris Lamb added and categorised a new [`ocaml_cmti_files`](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/1b9ce3c9) toolchain issue.

[![]({{ "/images/reports/2020-05/alpine.png#right" | relative_url }})](https://www.opensuse.org/)

In [Alpine Linux](https://alpinelinux.org/), an issue was filed — and closed — regarding the [reproducibility of `.apk` packages](https://gitlab.alpinelinux.org/alpine/apk-tools/-/issues/10693).

Allan McRae of the [ArchLinux](https://www.archlinux.org/) project posted their third [*Reproducible builds progress report*](https://lists.archlinux.org/pipermail/arch-dev-public/2020-May/029981.html) to the [`arch-dev-public` mailing list](https://lists.archlinux.org/listinfo/arch-dev-public) which includes the following call for help:

> We also need help to investigate and fix the packages that fail to reproduce that we have not investigated as of yet.

In [openSUSE](https://www.opensuse.org/), Bernhard M. Wiedemann published his [monthly Reproducible Builds status update](https://lists.opensuse.org/opensuse-factory/2020-06/msg00003.html).

<br>

## Software development

#### [diffoscope](https://diffoscope.org)

[![]({{ "/images/reports/2020-05/diffoscope.png#right" | relative_url }})](https://diffoscope.org)

Chris Lamb made the changes listed below to [diffoscope](https://diffoscope.org), our in-depth and content-aware diff utility that can locate and diagnose reproducibility issues. He also prepared and uploaded versions `142`, `143`, `144`, `145` and `146` to Debian, PyPI, etc.

* Comparison improvements:

    * Improve fuzzy matching of JSON files as [`file`](https://en.wikipedia.org/wiki/File_(command)) now supports recognising JSON data. ([#106](https://salsa.debian.org/reproducible-builds/diffoscope/issues/106))
    * Refactor `.changes` and `.buildinfo` handling to show all details (including the [GnuPG](https://gnupg) header and footer components) even when referenced files are not present. ([#122](https://salsa.debian.org/reproducible-builds/diffoscope/issues/122))
    * Use our `BuildinfoFile` comparator (etc.) regardless of whether the associated files (such as the `orig.tar.gz` and the `.deb`) are present. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/5f8952a)]
    * Include [GnuPG](https://gnupg.org/) signature data when comparing `.buildinfo`, `.changes`, etc. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/f35cac5)]
    * Add support for printing [Android APK](https://en.wikipedia.org/wiki/Android_application_package) signatures via `apksigner(1)`. ([#121](https://salsa.debian.org/reproducible-builds/diffoscope/issues/121))
    * Identify "iOS App Zip archive data" as `.zip` files. ([#116](https://salsa.debian.org/reproducible-builds/diffoscope/issues/116))
    * Add support for [Apple Xcode](https://en.wikipedia.org/wiki/Xcode) `.mobilepovision` files. ([#113](https://salsa.debian.org/reproducible-builds/diffoscope/issues/113))

* Bug fixes:

    * Don't print a traceback if we pass a single, missing argument to diffoscope (eg. a JSON diff to re-load). [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/1756a48)]
    * Correct `differences` typo in the `ApkFile` handler. ([#127](https://salsa.debian.org/reproducible-builds/diffoscope/issues/127))

* Output improvements:

    * Never emit the same `id="foo"` anchor reference twice in the HTML output, otherwise identically-named parts will not be able to linked to via a `#foo` anchor. ([#120](https://salsa.debian.org/reproducible-builds/diffoscope/issues/120))
    * Never emit an empty "id" anchor either; it is not possible to link to `#`. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/26e57c7)]
    * Don't pretty-print the output when using the `--json` presenter; it will usually be too complicated to be readable by the human anyway. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/443e2ba)]
    * Use the SHA256 over MD5 hash when generating page names for the HTML directory-style presenter. ([#124](https://salsa.debian.org/reproducible-builds/diffoscope/issues/124))

* Reporting improvements:

    * Clarify the message when we truncate the number of lines to standard error [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/ca9d589)] and reduce the number of maximum lines printed to 25 as usually the error is obvious by then [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/e1c29fc)].
    * Print the amount of free space that we have available in our temporary directory as a debugging message. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/090eece)]
    * Clarify `Command […] failed with exit code` messages to remove duplicate `exited with exit` but also to note that `diffoscope` is interpreting this as an error. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/ed8be4a)]
    * Don't leak the full path of the temporary directory in `Command […] exited with 1` messages. ([#126](https://salsa.debian.org/reproducible-builds/diffoscope/issues/126))
    * Clarify the warning message when we cannot import the `debian` Python module. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/e2a21f1)]
    * Don't repeat `stderr from {}` if both commands emit the same output. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/a294ad1)]
    * Clarify that an external command emits for both files, otherwise it can look like we are repeating itself when, in reality, it is being run twice. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/6d269b1)]

* Testsuite improvements:

    * Prevent `apksigner` test failures due to lack of `binfmt_misc`, eg. on Salsa CI and elsewhere. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/f101c91)]
    * Drop `.travis.yml` as we use [Salsa](https://salsa.debian.org/) instead. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/591ea6e)]

* `Dockerfile` improvements:

    * Add a `.dockerignore` file to whitelist files we actually need in our container. ([#105](https://salsa.debian.org/reproducible-builds/diffoscope/issues/105))
    * Use `ARG` instead of `ENV` when setting up the `DEBIAN_FRONTEND` environment variable at runtime. ([#103](https://salsa.debian.org/reproducible-builds/diffoscope/issues/103))
    * Run as a non-root user in container. ([#102](https://salsa.debian.org/reproducible-builds/diffoscope/issues/102))
    * Install/remove the `build-essential` during build so we can install the recommended packages from Git. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/e779fe6)]

* Codebase improvements:

    * Bump the officially required version of Python from 3.5 to 3.6. ([#117](https://salsa.debian.org/reproducible-builds/diffoscope/issues/117))
    * Drop the (default) `shell=False` keyword argument to `subprocess.Popen` so that the potentially-unsafe `shell=True` is more obvious. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/39c9831)]
    * Perform string normalisation in [Black](https://github.com/psf/black) [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/5b8e075)] and include the Black output in the assertion failure too [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/d743437)].
    * Inline `MissingFile`'s special handling of `deb822` to prevent leaking through abstract layers. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/ca8861d)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/8f63a6f)]
    * Allow a bare `try`/`except` block when cleaning up temporary files with respect to the [`flake8`](https://flake8.pycqa.org/en/latest/) quality assurance tool. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/f02ef5f)]
    * Rename `in_dsc_path` to `dsc_in_same_dir` to clarify the use of this variable. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/1e25a08)]
    * Abstract out the duplicated parts of the `debian_fallback` class [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/6db6cae)] and add descriptions for the file types. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/8b33709)]
    * Various commenting and internal documentation improvements. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/05b33f4)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/13dff09)]
    * Rename the `Openssl` command class to `OpenSSLPKCS7` to accommodate other command names with this prefix. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/b273af4)]

* Misc:

    * Rename the `--debugger` command-line argument to `--pdb`. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/6b2f2e4)]
    * Normalise filesystem `stat(2)` "birth times" (ie. `st_birthtime`) in the same way we do with the `stat(1)` command's `Access:` and `Change:` times to fix a nondeterministic build failure in [GNU Guix](https://guix.gnu.org/). ([#74](https://salsa.debian.org/reproducible-builds/diffoscope/issues/74))
    * Ignore case when ordering our file format descriptions. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/d701ff6)]
    * Drop, add and tidy various module imports. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/e799eff)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/dc4516b)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/1ed02bc)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/e6ce669)]

In addition:

* Jean-Romain Garnier fixed a general issue where, for example, `LibarchiveMember`'s `has_same_content` method was called regardless of the underlying type of file.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/c15e1f4)]

* Daniel Fullmer fixed an issue where some filesystems could only be mounted read-only.&nbsp;([!49](https://salsa.debian.org/reproducible-builds/diffoscope/-/merge_requests/49))

* Emanuel Bronshtein provided a patch to prevent a build of the [Docker](https://www.docker.com/) image containing parts of the build's.&nbsp;([#123](https://salsa.debian.org/reproducible-builds/diffoscope/issues/123))

* Mattia Rizzolo added an entry to `debian/py3dist-overrides` to ensure the `rpm-python` module is used in package dependencies&nbsp;([#89](https://salsa.debian.org/reproducible-builds/diffoscope/issues/89)) and moved to using the new `execute_after_*` and `execute_before_*` Debhelper rules&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/f469abc)].

<br>

[![]({{ "/images/reports/2020-05/diffoscope-website.png#right" | relative_url  }})](https://reproducible-builds.org/)

Chris Lamb also performed a huge overhaul of [diffoscope's website](https://diffoscope.org/):

* Add a completely new design. [[...](https://salsa.debian.org/reproducible-builds/diffoscope-website/commit/de6c072)][[...](https://salsa.debian.org/reproducible-builds/diffoscope-website/commit/7f38edd)]
* Dynamically generate our contributor list [[...](https://salsa.debian.org/reproducible-builds/diffoscope-website/commit/496ef9a)] and supported file formats [[...](https://salsa.debian.org/reproducible-builds/diffoscope-website/commit/f0663bb)] from the main Git repository.
* Add a separate, canonical page for every new release. [[...](https://salsa.debian.org/reproducible-builds/diffoscope-website/commit/4c25ae5)][[...](https://salsa.debian.org/reproducible-builds/diffoscope-website/commit/c4b6663)][[...](https://salsa.debian.org/reproducible-builds/diffoscope-website/commit/b87f063)]
* Generate a 'latest release' section and display that with the corresponding date on the homepage. [[...](https://salsa.debian.org/reproducible-builds/diffoscope-website/commit/eb8dad4)]
* Add an [RSS feed of our releases](https://diffoscope.org/feed.xml) [[...](https://salsa.debian.org/reproducible-builds/diffoscope-website/commit/4d25fb0)][[...](https://salsa.debian.org/reproducible-builds/diffoscope-website/commit/a0aae1f)][[...](https://salsa.debian.org/reproducible-builds/diffoscope-website/commit/40605de)][[...](https://salsa.debian.org/reproducible-builds/diffoscope-website/commit/8a0f8e0)][[...](https://salsa.debian.org/reproducible-builds/diffoscope-website/commit/e022562)] and add to [Planet Debian](https://planet.debian.org/) [[...](https://salsa.debian.org/planet-team/config/commit/5ad82fcd61bfed6a70cfa60898b37c4f4a855f1a)].
* Use Jekyll's `absolute_url` and `relative_url` where possible [[...](https://salsa.debian.org/reproducible-builds/diffoscope-website/commit/0e56f3c)][[...](https://salsa.debian.org/reproducible-builds/diffoscope-website/commit/bccda6e)] and move a number of configuration variables to `_config.yml` [[...](https://salsa.debian.org/reproducible-builds/diffoscope-website/commit/c0acbe9)][[...](https://salsa.debian.org/reproducible-builds/diffoscope-website/commit/6ea9f53)].

<br>

#### Upstream patches

The Reproducible Builds project detects, dissects and attempts to fix as many currently-unreproducible packages as possible. We endeavour to send all of our patches upstream where appropriate. This month, we wrote a large number of such patches, including:

* Bernhard M. Wiedemann:

    * [`golang-packaging`](https://github.com/openSUSE/golang-packaging/pull/30) (toolchain issue, affecting times in `minikube`)
    * [`jboss-logging-tools`](https://github.com/jboss-logging/jboss-logging-tools/pull/81) (toolchain issue, affecting date for `resteasy`)
    * [`linux_logo`](https://github.com/deater/linux_logo/pull/17) (sort `find` output to avoid inheriting filesystem order)
    * [`moonjit`](https://github.com/moonjit/moonjit/pull/124) (generate reproducible output by default if [`SOURCE_DATE_EPOCH`]({{ "/docs/source-date-epoch/" | relative_url }}) is set)
    * [`vala`](https://gitlab.gnome.org/GNOME/vala/-/issues/990) (report [ASLR](https://en.wikipedia.org/wiki/Address_space_layout_randomization) nondeterminism)

* Jelle van der Waa:

   * [`earlyoom`](https://github.com/rfjakob/earlyoom/pull/201) (timestamps in Gzip files)
   * [`fmt`](https://github.com/fmtlib/fmt/pull/1706) (Don't install `sphinx-build` cached files as they are unneeded & unreproducible)
   * [`nvidia-settings`](https://github.com/NVIDIA/nvidia-settings/pull/54) (timestamp in Gzip files)

* Chris Lamb:

    * [#959714](https://bugs.debian.org/959714) filed against [`ataqv`](https://tracker.debian.org/pkg/ataqv).
    * [#960313](https://bugs.debian.org/960313) filed against [`elinks`](https://tracker.debian.org/pkg/elinks).
    * [#960386](https://bugs.debian.org/960386) filed against [`briquolo`](https://tracker.debian.org/pkg/briquolo).
    * [#960388](https://bugs.debian.org/960388) filed against [`cryptominisat`](https://tracker.debian.org/pkg/cryptominisat).
    * [#960590](https://bugs.debian.org/960590) filed against [`wolfssl`](https://tracker.debian.org/pkg/wolfssl).
    * [#960591](https://bugs.debian.org/960591) filed against [`mistral`](https://tracker.debian.org/pkg/mistral).
    * [#960607](https://bugs.debian.org/960607) filed against [`python-watcherclient`](https://tracker.debian.org/pkg/python-watcherclient).
    * [#960669](https://bugs.debian.org/960669) filed against [`tree-puzzle`](https://tracker.debian.org/pkg/tree-puzzle).
    * [#961009](https://bugs.debian.org/961009) filed against [`nulib2`](https://tracker.debian.org/pkg/nulib2).
    * [#961202](https://bugs.debian.org/961202) filed against [`process-cpp`](https://tracker.debian.org/pkg/process-cpp).
    * [#961494](https://bugs.debian.org/961494) filed against [`bowtie2`](https://tracker.debian.org/pkg/bowtie2).
    * [#961495](https://bugs.debian.org/961495) filed against [`properties-cpp`](https://tracker.debian.org/pkg/properties-cpp).
    * [#961582](https://bugs.debian.org/961582) filed against [`wand`](https://tracker.debian.org/pkg/wand) ([forwarded upstream](https://github.com/emcconville/wand/pull/484))
    * [#961657](https://bugs.debian.org/961657) filed against [`vows`](https://tracker.debian.org/pkg/vows).

* Vagrant Cascadian:

    * [#961747](https://bugs.debian.org/961747) filed against [`libstatgrab`](https://tracker.debian.org/pkg/libstatgrab).
    * [#961764](https://bugs.debian.org/961764) filed against [`texi2html`](https://tracker.debian.org/pkg/texi2html).
    * [#961766](https://bugs.debian.org/961766) filed against [`grub`](https://tracker.debian.org/pkg/grub).
    * [#961830](https://bugs.debian.org/961830) filed against [`systemtap`](https://tracker.debian.org/pkg/systemtap).
    * [#961942](https://bugs.debian.org/961942) filed against [`mono`](https://tracker.debian.org/pkg/mono).
    * [`mescc-tools`](https://github.com/oriansj/mescc-tools/pull/10): Inherit `CFLAGS` in a `Makefile`, allowing `-ffile-prefix-map`/`-fdebug-prefix-map` to sanitise build paths ([merged upstream](https://github.com/oriansj/mescc-tools/commit/37f3ce4278770f42da06d9dddab9d0f8148998e2)).

#### Other tools

Elsewhere in our tooling:

[strip-nondeterminism](https://tracker.debian.org/pkg/strip-nondeterminism) is our tool to remove specific non-deterministic results from a completed build. In May, Chris Lamb uploaded version `1.8.1-1` to Debian *unstable* and Bernhard M. Wiedemann fixed an "off-by-one" error when parsing PNG image modification times. ([#16](https://salsa.debian.org/reproducible-builds/strip-nondeterminism/commit/4c3d64b))

In [disorderfs](https://tracker.debian.org/pkg/disorderfs), our [FUSE](https://en.wikipedia.org/wiki/Filesystem_in_Userspace)-based filesystem that deliberately introduces non-determinism into directory system calls in order to flush out reproducibility issues, Chris Lamb replaced the term "dirents" in place of "directory entries" in human-readable output/log messages [[...](https://salsa.debian.org/reproducible-builds/disorderfs/commit/89ab46e)] and used the [astyle](http://astyle.sourceforge.net/) source code formatter with the default settings to the main `disorderfs.cpp` source file [[...](https://salsa.debian.org/reproducible-builds/disorderfs/commit/119c27c)].

Holger Levsen bumped the `debhelper-compat level` to 13 in *disorderfs* [[...](https://salsa.debian.org/reproducible-builds/disorderfs/commit/d1b7aa7)] and *reprotest* [[...]([[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/f2decef)])], and for the [GNU Guix](https://guix.gnu.org/) distribution Vagrant Cascadian updated the versions of *disorderfs* to version 0.5.10 [[...](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=2f04adf5c4e4be8e44e2bf753d4e0a87f9149c7e)] and *diffoscope* to version 145 [[...](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=0321c6ebeae35709cde65b2dccd0a3a6a5d6aeee)].

#### Project documentation & website

[![]({{ "/images/reports/2020-05/website.png#right" | relative_url }})]({{ "/" | relative_url }})

* Carl Dong:

    * Clarify some potential confusion around [GCC `libtool`](https://www.gnu.org/software/libtool/).&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/b95a093)]

* Chris Lamb:

    * Rename the *Who* page to [*Projects*]({{ "/projects/" | relative_url }})". [[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/cfd867d)]
    * Ensure that [Jekyll](https://jekyllrb.com/) enters the `_docs` subdirectory to find the `_docs/index.md` file after an internal move. ([#27](https://salsa.debian.org/reproducible-builds/reproducible-website/issues/27))
    * Wrap `ltmain.sh` etc. in preformatted quotes. [[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/baa6eb9)]
    * Wrap the [`SOURCE_DATE_EPOCH` Python examples]({{ "/docs/source-date-epoch/#python" | relative_url }}) onto more lines to prevent visual overflow on the page. [[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/d376d14)]
    * Correct a "preferred" spelling error. [[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/47c3d52)]

* Holger Levsen:

    * Sort our [Academic publications]({{ "/docs/publications/" | relative_url }}) page by publication year&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/97daeef)] and add "Trusting Trust" and "Fully Countering Trusting Trust through Diverse Double-Compiling"&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/e6724b5)].

* Juri Dispan:

    * Update the URL for `faketime` to the [project's Github page](https://github.com/wolfcw/libfaketime).&nbsp;([!57](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/80f0d82))

#### Testing framework

[![]({{ "/images/reports/2020-05/testframework.png#right" | relative_url }})](https://tests.reproducible-builds.org/)

We operate a large and many-featured [Jenkins](https://jenkins.io/)-based testing framework that powers [`tests.reproducible-builds.org`](https://tests.reproducible-builds.org) that, amongst many other tasks, tracks the status of our reproducibility efforts as well as identifies any regressions that have been introduced. Holger Levsen made the following changes:

* System health status:

    * Improve page description.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/3dc2fba8)]
    * Add more weight to proxy failures.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/5eee3d08)]
    * More verbose debug/failure messages.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/e741ba01)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/5c35c4bb)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/74533631)]
    * Work around strangeness in the [Bash](https://www.gnu.org/software/bash/) shell — `let VARIABLE=0` exits with an error.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/6f2da124)]

* [Debian](https://debian.org/):

    * Fail loudly if there are more than three `.buildinfo` files with the same name.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/62fee324)]
    * Fix a typo which prevented [`/usr` merge](https://wiki.debian.org/UsrMerge) variation on Debian *unstable*.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/4d69f583)]
    * Temporarily ignore PHP's horde](https://www.horde.org/) packages in Debian *bullseye*.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/7a604926)]
    * Document how to reboot all nodes in parallel, working around [`molly-guard`](https://packages.debian.org/sid/admin/molly-guard).&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/644a4755)]

* Further work on a Debian package rebuilder:

    * Workaround and document various issues in the `debrebuild` script.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/e1b6201c)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/26bf92ba)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/b89d482f)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/cb5aeaf9)]
    * Improve output in the case of errors.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/4a364fb7)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/54d8e59f)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/c6783a8e)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/bb2dc461)]
    * Improve documentation and future goals&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/2ac104da)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/09c24a46)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/fe8c07e2)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/c6143fa2)], in particular documentiing two real world tests case for an "impossible to recreate build environment"&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/98347cff)].
    * Find the right source package to rebuild.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/80636eac)]
    * Increase the frequency we run the script.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/e3c7b0a2)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/6a9110c1)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/4f6a9dc7)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/54cf39c6)]
    * Improve downloading and selection of the sources to build.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/af874937)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/cbdd243a)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/37a824e0)]
    * Improve version string handling..&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/c37ad851)]
    * Handle build failures better.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/ba2e9efd)].&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/47aec708)].&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/6a24b95c)]
    * Also consider "architecture all" `.buildinfo` files.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/fc8c2c3c)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/f8a41fe6)]

In addition:

[![]({{ "/images/reports/2020-05/alpine.png#right" | relative_url }})](https://www.opensuse.org/)

* *kpcyrd*, for [Alpine Linux](https://alpinelinux.org/), updated the `alpine_schroot.sh` script now that a patch for `abuild` had been released upstream.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/b24e02e6)]

* Alexander Couzens of the [OpenWrt](https://openwrt.org/) project renamed the `brcm47xx` target to `bcm47xx`.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/fbe011d7)]

* Mattia Rizzolo fixed the printing of the build environment during the second build&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/72cf73c4)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/38479bc3)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/0ed346e5)] and made a number of improvements to the script that deploys [Jenkins](https://www.jenkins.io/) across our infrastructure&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/df936410)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/f89b8d88)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/9086c3a8)].

Lastly, Vagrant Cascadian clarified in the documentation that you need to be user `jenkins` to run the `blacklist` command&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/7b747291)] and the usual build node maintenance was performed by Holger Levsen [[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/fb73c0fd)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/a02ffc3e)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/0b84e560)], Mattia Rizzolo [[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/4ade95a2)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/38e83e9a)] and Vagrant Cascadian [[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/e48df377)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/b54b5705)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/4d53cded)].

<br>

## [Mailing list](https://lists.reproducible-builds.org/listinfo/rb-general/):

There were a number of discussions on [our mailing list](https://lists.reproducible-builds.org/listinfo/rb-general/) this month:

[![]({{ "/images/reports/2020-05/verification-format-thread.png#right" | relative_url }})](https://lists.reproducible-builds.org/pipermail/rb-general/2020-May/thread.html#1922)

Paul Spooren started a thread titled [*Reproducible Builds Verification Format*](https://lists.reproducible-builds.org/pipermail/rb-general/2020-May/001922.html) which reopens the discussion around a schema for sharing the results from distributed rebuilders:

> To make the results accessible, storable and create tools around them, they should all follow the same schema, a *reproducible builds verification format*. The format tries to be as generic as possible to cover all open source projects offering precompiled source code. It stores the rebuilder results of what is reproducible and what not.

Hans-Christoph Steiner of the [Guardian Project](https://guardianproject.info/) also continued [his previous discussion](https://lists.reproducible-builds.org/pipermail/rb-general/2020-April/001893.html) regarding [making our website translatable](https://lists.reproducible-builds.org/pipermail/rb-general/2020-May/001916.html).

Lastly, Leo Wandersleb posted a detailed request for feedback on [a question of supply chain security and other issues of software review](https://lists.reproducible-builds.org/pipermail/rb-general/2020-May/001949.html); Leo is the founder of the [Wallet Scrutiny](https://walletscrutiny.com/) project which aims to prove the security of Android Bitcoin Wallets:

> Do you own your Bitcoins or do you trust that your app allows you to use "your" coins while they are actually controlled by “them"? Do you have a backup? Do “they" have a copy they didn’t tell you about? Did anybody check the wallet for deliberate backdoors or vulnerabilities? Could anybody check the wallet for those?

Elsewhere, Leo had posted instructions on [his attempts to reproduce the binaries](https://github.com/BlueWallet/BlueWallet/issues/758#issuecomment-632083452) for the [BlueWallet](https://bluewallet.io/) Bitcoin wallet for iOS and Android platforms.

<br>
<hr>
<br>

If you are interested in contributing to the Reproducible Builds project, please visit our [*Contribute*](https://reproducible-builds.org/contribute/) page on our website. However, you can get in touch with us via:

 * IRC: `#reproducible-builds` on `irc.oftc.net`.

 * Twitter: [@ReproBuilds](https://twitter.com/ReproBuilds)

 * Reddit: [/r/ReproducibleBuilds](https://reddit.com/r/reproduciblebuilds)

 * Mailing list: [`rb-general@lists.reproducible-builds.org`](https://lists.reproducible-builds.org/listinfo/rb-general)

<br>

This month's report was written by Bernhard M. Wiedemann, Chris Lamb, Holger Levsen, Jelle van der Waa and Vagrant Cascadian. It was subsequently reviewed by a bunch of Reproducible Builds folks on IRC and the mailing list.
{: .small}
