---
layout: default
title: Contribute to NixOS
permalink: /contribute/nixos/
---

## Community Discussion

For real-time conversations, join the [`#reproducible-builds:nixos.org`](https://matrix.to/#/#reproducible-builds:nixos.org) channel on Matrix.

## Project Status

- **Project Board**: To keep track of ongoing tasks and open issues concerning
  reproducible builds, view our [GitHub Project Board](https://github.com/orgs/NixOS/projects/30).
- **Pull Requests & Issues**: Browse current [pull requests](https://github.com/NixOS/nixpkgs/pulls?q=is%3Aopen+is%3Apr+label%3A%226.topic%3A+reproducible+builds%22) and [issues](https://github.com/NixOS/nixpkgs/issues?q=is%3Aopen+is%3Aissue+label%3A%226.topic%3A+reproducible+builds%22) labeled with "reproducible builds."
- **Internal Resources**: Our internal website, [reproducible.nixos.org](https://reproducible.nixos.org), provides additional information and status updates. More details on specific problems can be found on [this pad](https://pad.sfconservancy.org/p/nixos-reproducible-builds-progress).

## Reporting Issues

Use the [issue template](https://github.com/NixOS/nixpkgs/issues/new?assignees=&labels=0.kind%3A+enhancement%2C6.topic%3A+reproducible+builds&template=unreproducible_package.md&title=) on GitHub to report your issues and hopefully, your solution.

## Additional Projects

- [trustix](https://github.com/nix-community/trustix)
